A buffer overflow occurs when more data is written to a buffer (a region of memory used to hold data temporarily) than the buffer is designed to hold. Here's an example:

Suppose we have a program that reads a user's name and stores it in a buffer:
```
#include <stdio.h>
#include <string.h>

int main() {
    char name[10]; // buffer to store the user's name

        printf("Enter your name: ");
            gets(name); // read user input into the buffer

                printf("Hello, %s!\n", name);
                    return 0;
                    }
                    ```
                    The `gets` function reads input from the user and stores it in the `name` buffer. However, `gets` does not check the length of the input, so if the user enters a name longer than 10 characters, it will overflow the buffer.

                    For example, if the user enters the string "abcdefghijklmnopqrstuvwxyz", the buffer will overflow, and the extra characters will overwrite adjacent areas of memory.

                    This can lead to unexpected behavior, crashes, or even code execution attacks.

                    To fix this vulnerability, we can use the `fgets` function instead, which allows us to specify the maximum number of characters to read:
                    ```
                    #include <stdio.h>
                    #include <string.h>

                    int main() {
                        char name[10]; // buffer to store the user's name

                            printf("Enter your name: ");
                                fgets(name, 10, stdin); // read user input into the buffer, max 10 chars

                                    printf("Hello, %s!\n", name);
                                        return 0;
                                        }
                                        ```
                                        This way, we ensure that the buffer will not overflow, and the program will behave as expected.